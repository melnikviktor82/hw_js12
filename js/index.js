"use strict";

// ## Теоретический вопрос

// 1. Почему для работы с input не рекомендуется использовать события клавиатуры?

// На сучасних пристроях є інші способи «ввести щось».
// Тому, якщо ми хочемо коректно відстежувати введення в поле <input>, то одних клавіатурних подій недостатньо.



const btnList = Array.from(document.querySelectorAll(".btn"));

document.addEventListener("keydown", (event) => {
  btnList.forEach((elem) => {
    elem.style.backgroundColor = "#000000";
  });
  btnList.forEach((elem) => {
    if (
      elem.innerText === event.code ||
      event.code === `Key${elem.innerText}`
    ) {
      elem.style.backgroundColor = "blue";
    }
  });
});
